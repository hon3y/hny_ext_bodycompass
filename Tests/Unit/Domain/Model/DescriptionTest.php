<?php
namespace HIVE\HiveExtBodycompass\Tests\Unit\Domain\Model;

/**
 * Test case.
 *
 * @author Andreas Hafner <a.hafner@teufels.com>
 * @author Dominik Hilser <d.hilser@teufels.com>
 * @author Georg Kathan <g.kathan@teufels.com>
 * @author Hendrik Krüger <h.krueger@teufels.com>
 * @author Josymar Escalona Rodriguez <j.rodriguez@teufels.com>
 * @author Perrin Ennen <p.ennen@teufels.com>
 * @author Timo Bittner <t.bittner@teufels.com>
 */
class DescriptionTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \HIVE\HiveExtBodycompass\Domain\Model\Description
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \HIVE\HiveExtBodycompass\Domain\Model\Description();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getBackendtitleReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getBackendtitle()
        );
    }

    /**
     * @test
     */
    public function setBackendtitleForStringSetsBackendtitle()
    {
        $this->subject->setBackendtitle('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'backendtitle',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getTitleReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getTitle()
        );
    }

    /**
     * @test
     */
    public function setTitleForStringSetsTitle()
    {
        $this->subject->setTitle('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'title',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getContentReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getContent()
        );
    }

    /**
     * @test
     */
    public function setContentForStringSetsContent()
    {
        $this->subject->setContent('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'content',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getBtnReturnsInitialValueForBtn()
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getBtn()
        );
    }

    /**
     * @test
     */
    public function setBtnForObjectStorageContainingBtnSetsBtn()
    {
        $btn = new \HIVE\HiveCptCntBsBtn\Domain\Model\Btn();
        $objectStorageHoldingExactlyOneBtn = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOneBtn->attach($btn);
        $this->subject->setBtn($objectStorageHoldingExactlyOneBtn);

        self::assertAttributeEquals(
            $objectStorageHoldingExactlyOneBtn,
            'btn',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function addBtnToObjectStorageHoldingBtn()
    {
        $btn = new \HIVE\HiveCptCntBsBtn\Domain\Model\Btn();
        $btnObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $btnObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($btn));
        $this->inject($this->subject, 'btn', $btnObjectStorageMock);

        $this->subject->addBtn($btn);
    }

    /**
     * @test
     */
    public function removeBtnFromObjectStorageHoldingBtn()
    {
        $btn = new \HIVE\HiveCptCntBsBtn\Domain\Model\Btn();
        $btnObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $btnObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($btn));
        $this->inject($this->subject, 'btn', $btnObjectStorageMock);

        $this->subject->removeBtn($btn);
    }

    /**
     * @test
     */
    public function getRelatedEntryReturnsInitialValueForEntry()
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getRelatedEntry()
        );
    }

    /**
     * @test
     */
    public function setRelatedEntryForObjectStorageContainingEntrySetsRelatedEntry()
    {
        $relatedEntry = new \HIVE\HiveExtBodycompass\Domain\Model\Entry();
        $objectStorageHoldingExactlyOneRelatedEntry = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOneRelatedEntry->attach($relatedEntry);
        $this->subject->setRelatedEntry($objectStorageHoldingExactlyOneRelatedEntry);

        self::assertAttributeEquals(
            $objectStorageHoldingExactlyOneRelatedEntry,
            'relatedEntry',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function addRelatedEntryToObjectStorageHoldingRelatedEntry()
    {
        $relatedEntry = new \HIVE\HiveExtBodycompass\Domain\Model\Entry();
        $relatedEntryObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $relatedEntryObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($relatedEntry));
        $this->inject($this->subject, 'relatedEntry', $relatedEntryObjectStorageMock);

        $this->subject->addRelatedEntry($relatedEntry);
    }

    /**
     * @test
     */
    public function removeRelatedEntryFromObjectStorageHoldingRelatedEntry()
    {
        $relatedEntry = new \HIVE\HiveExtBodycompass\Domain\Model\Entry();
        $relatedEntryObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $relatedEntryObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($relatedEntry));
        $this->inject($this->subject, 'relatedEntry', $relatedEntryObjectStorageMock);

        $this->subject->removeRelatedEntry($relatedEntry);
    }
}
