<?php
namespace HIVE\HiveExtBodycompass\Domain\Model;

/***
 *
 * This file is part of the "hive_ext_bodycompass" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

/**
 * Description
 */
class Description extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * backendtitle
     *
     * @var string
     * @validate NotEmpty
     */
    protected $backendtitle = '';

    /**
     * title
     *
     * @var string
     * @validate NotEmpty
     */
    protected $title = '';

    /**
     * content
     *
     * @var string
     */
    protected $content = '';

    /**
     * btn
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\HIVE\HiveCptCntBsBtn\Domain\Model\Btn>
     */
    protected $btn = null;

    /**
     * relatedEntry
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\HIVE\HiveExtBodycompass\Domain\Model\Entry>
     */
    protected $relatedEntry = null;

    /**
     * __construct
     */
    public function __construct()
    {
        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }

    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     *
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->btn = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->relatedEntry = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

    /**
     * Returns the backendtitle
     *
     * @return string $backendtitle
     */
    public function getBackendtitle()
    {
        return $this->backendtitle;
    }

    /**
     * Sets the backendtitle
     *
     * @param string $backendtitle
     * @return void
     */
    public function setBackendtitle($backendtitle)
    {
        $this->backendtitle = $backendtitle;
    }

    /**
     * Returns the title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     *
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Returns the content
     *
     * @return string $content
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Sets the content
     *
     * @param string $content
     * @return void
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * Adds a Btn
     *
     * @param \HIVE\HiveCptCntBsBtn\Domain\Model\Btn $btn
     * @return void
     */
    public function addBtn(\HIVE\HiveCptCntBsBtn\Domain\Model\Btn $btn)
    {
        $this->btn->attach($btn);
    }

    /**
     * Removes a Btn
     *
     * @param \HIVE\HiveCptCntBsBtn\Domain\Model\Btn $btnToRemove The Btn to be removed
     * @return void
     */
    public function removeBtn(\HIVE\HiveCptCntBsBtn\Domain\Model\Btn $btnToRemove)
    {
        $this->btn->detach($btnToRemove);
    }

    /**
     * Returns the btn
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\HIVE\HiveCptCntBsBtn\Domain\Model\Btn> $btn
     */
    public function getBtn()
    {
        return $this->btn;
    }

    /**
     * Sets the btn
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\HIVE\HiveCptCntBsBtn\Domain\Model\Btn> $btn
     * @return void
     */
    public function setBtn(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $btn)
    {
        $this->btn = $btn;
    }

    /**
     * Adds a Entry
     *
     * @param \HIVE\HiveExtBodycompass\Domain\Model\Entry $relatedEntry
     * @return void
     */
    public function addRelatedEntry(\HIVE\HiveExtBodycompass\Domain\Model\Entry $relatedEntry)
    {
        $this->relatedEntry->attach($relatedEntry);
    }

    /**
     * Removes a Entry
     *
     * @param \HIVE\HiveExtBodycompass\Domain\Model\Entry $relatedEntryToRemove The Entry to be removed
     * @return void
     */
    public function removeRelatedEntry(\HIVE\HiveExtBodycompass\Domain\Model\Entry $relatedEntryToRemove)
    {
        $this->relatedEntry->detach($relatedEntryToRemove);
    }

    /**
     * Returns the relatedEntry
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\HIVE\HiveExtBodycompass\Domain\Model\Entry> $relatedEntry
     */
    public function getRelatedEntry()
    {
        return $this->relatedEntry;
    }

    /**
     * Sets the relatedEntry
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\HIVE\HiveExtBodycompass\Domain\Model\Entry> $relatedEntry
     * @return void
     */
    public function setRelatedEntry(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $relatedEntry)
    {
        $this->relatedEntry = $relatedEntry;
    }
}
